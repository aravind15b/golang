package main

import "fmt"

func runningSum(nums []int) []int {
	var sum int
	var res []int
	for _, no := range nums {
		sum += no
		res = append(res, sum)
	}
	return res
}

func main() {
	// sum := runningSum([]int{1, 1, 1, 1, 1})
	// sum := runningSum([]int{1, 2, 3, 4})
	sum := runningSum([]int{3, 1, 2, 10, 1})
	fmt.Println(sum)
}
