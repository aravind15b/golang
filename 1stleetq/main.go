package main

import "fmt"

func sum1(numbers []int, target int) []int {
	for i, no1 := range numbers {
		for j, no2 := range numbers {
			if no1+no2 == target && i != j {
				return []int{i, j}
			}
		}
	}

	return nil
}

func main() {
	numbers := []int{4, 3, 3}
	fmt.Println(sum1(numbers, 7))
}
