package main

import "fmt"

func pivotIndex(nums []int) int {
	var list []int
	var sum, sumL int
	keys := make(map[int]bool)
	for _, v := range nums {
		sum += v
	}
	for i, v := range nums {
		if _, value := keys[v]; !value {
			keys[v] = true
			list = append(list, v)
		}
		sumL += v
		if sumL-v == sum-sumL {
			return i
		}
	}
	return -1
}

func main() {
	index := pivotIndex([]int{1, 7, 3, 6, 5, 6})
	fmt.Println(index)
}
