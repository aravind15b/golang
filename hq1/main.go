package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

func plusMinus(arr []int32) (string, string, string) {
	// Write your code here
	var posCount, zeroCount, negCount int
	for i := range arr {
		if arr[i] == 0 {
			zeroCount++
		} else if math.Signbit(float64(arr[i])) {
			negCount++
		} else {
			posCount++
			if arr[i] == 0 {
				posCount--
			}
		}
	}
	pF := float64(posCount) / float64(len(arr))
	nF := float64(negCount) / float64(len(arr))
	zF := float64(zeroCount) / float64(len(arr))
	ps := fmt.Sprintf("%.6f\n", pF)
	ns := fmt.Sprintf("%.6f\n", nF)
	zs := fmt.Sprintf("%.6f\n", zF)
	return ps, ns, zs
}

func main() {
	reader := bufio.NewReaderSize(os.Stdin, 16*1024*1024)

	nTemp, err := strconv.ParseInt(strings.TrimSpace(readLine(reader)), 10, 64)
	checkError(err)
	n := int32(nTemp)

	arrTemp := strings.Split(strings.TrimSpace(readLine(reader)), " ")

	var arr []int32

	for i := 0; i < int(n); i++ {
		arrItemTemp, err := strconv.ParseInt(arrTemp[i], 10, 64)
		checkError(err)
		arrItem := int32(arrItemTemp)
		arr = append(arr, arrItem)
	}

	fmt.Println(plusMinus(arr))
}

func readLine(reader *bufio.Reader) string {
	str, _, err := reader.ReadLine()
	if err == io.EOF {
		return ""
	}

	return strings.TrimRight(string(str), "\r\n")
}

func checkError(err error) {
	if err != nil {
		panic(err)
	}
}
